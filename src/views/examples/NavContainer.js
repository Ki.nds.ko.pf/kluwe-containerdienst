import React, { useEffect, state } from "react";
import classnames from "classnames";
import { abfallsorten, containerList } from "./Data.js";
import Abfallsorte from "views/examples/Abfallsorte.js";
import ContainerAuswahl from "views/examples/ContainerAuswahl.js";
import Anfrage from "views/examples/Anfrage.js";
// reactstrap components
import {
    TabContent,
    TabPane,
    Container,
    Row,
    Col,
    Card,
    CardHeader,
    CardBody,
    CardFooter,
    Pagination,
    PaginationItem,
    PaginationLink,
    Nav,
    NavItem,
    NavLink,
    Progress,
    UncontrolledAlert,
    Button,
} from "reactstrap";

import { data } from "jquery";



export default function NavContainer() {
    const [iconTabs, setIconsTabs] = React.useState(1);
    const [progress, setProgress] = React.useState(0);
    const [pills, setPills] = React.useState(1);
    const [activeAbfallsorteId, setActiveAbfallsorteId] = React.useState(null);
    const [activeContainerId, setActiveContainerId] = React.useState(null);
    const [activeAbfallsorte, setActiveAbfallsorte] = React.useState({
        id: 0,
        src: "",
        name: '',
        beschreibung: "",
        preis: "",
        container: [],
        dos: [''],
        donts: [''],
    });
    const [activeContainer, setActiveContainer] = React.useState({
        id: null,
        src: "",
        art: '',
        volume: '',
        ladekante: '',
        preisProTonne: 0
    });

    useEffect(() => {
        if (iconTabs === 1) {
            setProgress(33)
        } else if (iconTabs === 2) {
            setProgress(66)
        } else if (iconTabs === 3) {
            setProgress(99)
        }
    });

    const pagination = (
        <Container>
            <Pagination
                className="pagination pagination-info"
                listClassName="pagination-info"
            >
                <PaginationItem>
                    <PaginationLink
                        aria-label="Previous"
                        href="#pablo"
                        onClick={(e) => {
                            if (pills === 1) {
                                setPills(3);
                                setIconsTabs(3);
                            } else {
                                setPills(pills - 1);
                                setIconsTabs(iconTabs - 1);
                            }
                        }}
                    >
                        <span aria-hidden={true}>
                            <i
                                aria-hidden={true}
                                className="tim-icons icon-double-left"
                            />
                            &nbsp; Zurück
                        </span>
                    </PaginationLink>
                </PaginationItem>
                <PaginationItem className={classnames({
                    "active show": pills === 1,
                })}>
                    <PaginationLink
                        href="#pablo"
                        className={classnames({
                            active: iconTabs === 1,
                            "active show": pills === 1,
                        })}
                        onClick={(e) => { setIconsTabs(1); setPills(1) }}
                    >
                        1
            </PaginationLink>
                </PaginationItem>
                <PaginationItem className={classnames({
                    "active show": pills === 2,
                })}>
                    <PaginationLink
                        href="#pablo"
                        className={classnames({
                            active: iconTabs === 2,
                            "active show": pills === 2,
                        })}
                        onClick={(e) => { setIconsTabs(2); setPills(2) }}
                    >
                        2
            </PaginationLink>
                </PaginationItem>
                <PaginationItem className={classnames({
                    "active show": pills === 3,
                })}>
                    <PaginationLink
                        href="#pablo"
                        className={classnames({
                            active: iconTabs === 3,
                            "active show": pills === 3,
                        })}
                        onClick={(e) => { setIconsTabs(3); setPills(3) }}
                    >
                        3
            </PaginationLink>
                </PaginationItem>
                <PaginationItem>
                    <PaginationLink
                        aria-label="Next"
                        href="#pablo"
                        onClick={(e) => {
                            if (pills === 3) {
                                setPills(1);
                                setIconsTabs(1);
                            } else {
                                setPills(pills + 1);
                                setIconsTabs(iconTabs + 1);
                            }
                        }}
                    >
                        <span aria-hidden={true}>
                            Weiter &nbsp;
                            <i
                                aria-hidden={true}
                                className="tim-icons icon-double-right"
                            />
                        </span>
                    </PaginationLink>
                </PaginationItem>
            </Pagination>
        </Container>
    );

    const changeActiveAbfallsorte = (id) => {
        setActiveAbfallsorteId(id);
        const abfallsorte = abfallsorten.find((e) => e.id === id);
        setActiveAbfallsorte(abfallsorte);
    }

    const changeActiveContainer = (id) => {
        setActiveContainerId(id);
        const container = containerList.find((e) => e.id === id);
        setActiveContainer(container);
    }

    const changeActiveTab = (num) => {
        setIconsTabs(1);
        setPills(1);
    }

    return (
        <div className="section section-tabs">
            <Container>
                <Row>
                    <Col className="ml-auto mr-auto" >
                        <Card>
                            <CardHeader>
                                <Nav className="nav-tabs-info" role="tablist" tabs>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({
                                                active: iconTabs === 1,
                                            })}
                                            onClick={(e) => { setIconsTabs(1); setPills(1) }}
                                            href="#pablo"
                                        >
                                            <i className="tim-icons icon-trash-simple" />
                        Abfallsorte
                      </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({
                                                active: iconTabs === 2,
                                            })}
                                            onClick={(e) => { setIconsTabs(2); setPills(2) }}
                                            href="#pablo"
                                        >
                                            <i className="tim-icons icon-delivery-fast" />
                        Container
                      </NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <NavLink
                                            className={classnames({
                                                active: iconTabs === 3,
                                            })}
                                            onClick={(e) => { setIconsTabs(3); setPills(3) }}
                                            href="#pablo"
                                        >
                                            <i className="tim-icons icon-send" />
                        Anfrage
                      </NavLink>
                                    </NavItem>
                                </Nav>
                            </CardHeader>
                            <CardBody>
                                <TabContent className="tab-space" activeTab={"link" + iconTabs}>
                                    <TabPane tabId="link1">
                                        <Abfallsorte changeActiveAbfallsorte={changeActiveAbfallsorte} ></Abfallsorte>
                                    </TabPane>
                                    <TabPane tabId="link2">
                                        <ContainerAuswahl id={activeAbfallsorteId} changeActiveContainer={changeActiveContainer} changeActiveTab={changeActiveTab}></ContainerAuswahl>
                                    </TabPane>
                                    <TabPane tabId="link3">
                                        <Anfrage abfallsorte={activeAbfallsorte} container={activeContainer}></Anfrage>
                                    </TabPane>
                                </TabContent>
                                <div className="progress-container progress-info"><Progress max="100" value={progress}>
                                </Progress></div>
                            </CardBody>

                            <CardFooter className="text-center pagination-container">

                                <div>{pagination}</div>

                            </CardFooter>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}