import React, { useEffect, state } from "react";
import classnames from "classnames";
import { abfallsorten, containerList } from "./Data.js";

// reactstrap components
import {
    Modal,
    Row,
    Col,
    Card,
    CardBody,
    CardImg,
    CardTitle,
    CardText,
    FormGroup,
    Input,
    Button,
    UncontrolledAlert,
} from "reactstrap";

export default function ContainerAuswahl(props) {
    const [activeContainerKey, setActiveContainerKey] = React.useState(null);
    if (props.id) {

        return (
            <Row>
                { abfallsorten.find((e) => props.id === e.id).container.map((containerId) => {
                        let container = containerList.find((e)=> containerId === e.id);
                        console.log(container.id);
                //     console.log("container:" + container);
                    return(
                        <Col sm="3">
                        <Card className={
                            `btn abfall card-abfallsorte ${activeContainerKey === containerId ? 'card-abfallsorte-active' : ''}`}
                            onClick={() => { setActiveContainerKey(containerId); props.changeActiveContainer(containerId); }}>
                            <CardImg variant="top" src={container.src}/>
                            <div class="centered"><h3>{container.volume}</h3></div>
                            <CardBody>
                                <CardTitle><strong>{container.art}</strong></CardTitle>
                                <CardText>
                                    {container.ladekante} <br/>
                                    Stellung: {container.preisProTonne}€ <br/>
                                    Maße: <br/>
                                    
                                </CardText>
                                <Button className="btn-link abfall card-abfallsorte-auswahl" color="info"
                                    onClick={() => { setActiveContainerKey(containerId); props.changeActiveContainer(containerId);  }}>
                                    auswählen
                                </Button>
                            </CardBody>
                        </Card>
                    </Col>
                    );
                })}
            </Row>
        );
    } else {
        return (
            <UncontrolledAlert className="alert-with-icon" color="default">
                <span data-notify="icon" className="tim-icons icon-support-17" />
                <span>
                    <b>Bitte wähle einen Abfallsorte auß -</b>
                    <Button className="btn-link" color="default"
                        onClick={(e) => { props.changeActiveTab(1) }}>
                        zurück zu Abfallsorte
                    </Button>
                </span>
            </UncontrolledAlert>
        );
    }

}