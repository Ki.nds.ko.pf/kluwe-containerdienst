import React, { Component } from 'react';
import { InlineWidget } from 'react-calendly'

export default function Calendly(props) {
    const prefill = {
        email: 'test@test.com',
        firstName: 'Jon',
        lastName: 'Snow',
        name: 'Containerbestellungen + name',
        customAnswers: {
            /* a1: 'a1', */
            a2: props.abfallsorte.name,
            a3: props.container.art + ", " + props.container.volume + ", " + props.container.ladekante,
            /*  a4: 'a4',
             a5: 'a5',
             a6: 'a6',
             a7: 'a7',
             a8: 'a8',
             a9: 'a9',
             a10: 'a10' */
        }
    }
    const utm = {
        utmCampaign: 'Spring Sale 2019',
        utmContent: 'Shoe and Shirts',
        utmMedium: 'Ad',
        utmSource: 'Facebook',
        utmTerm: 'Spring'
    }
    const styles = {
        height: '1000px'
    }
    const pageSettings = {
        backgroundColor: 'ffffff',
        hideEventTypeDetails: false,
        hideLandingPageDetails: false,
        primaryColor: '00a2ff',
        textColor: '4d5055'
    }

    return (
        <div>
            <div id="schedule_form">
                <InlineWidget
                    url="https://calendly.com/rlukas-2/30min?hide_gdpr_banner=1"
                    styles={styles}
                    pageSettings={pageSettings}
                    prefill={prefill}
                    utm={utm}
                />
                {/*                     <div
                        className="calendly-inline-widget"
                        data-url="https://calendly.com/rlukas-2/30min?hide_gdpr_banner=1"
                        style={{ minWidth: '320px', height: '630px' }} /> */}
            </div>
        </div>
    );
}