import React, { useEffect, state } from "react";
import classnames from "classnames";
import { abfallsorten } from "./Data.js";


// reactstrap components
import {
    Modal,
    Row,
    Col,
    Card,
    CardBody,
    CardImg,
    CardTitle,
    CardText,
    FormGroup,
    Input,
    Button,
} from "reactstrap";

export default function Abfallsorte(props) {

    const [activeAbfallsorteKey, setActiveAbfallsorteKey] = React.useState(null);
    const [demoModal, setDemoModal] = React.useState(false);
    const [activeAbfallsorte, setActiveAbfallsorte] = React.useState({
        id: null,
        src: null,
        name: null,
        beschreibung: null,
        preis: null,
        container: [],
        dos: [],
        donts: [],
    });

    const modal = () => (
        <Modal
            modalClassName="modal-black"
            isOpen={demoModal} toggle={() => setDemoModal(false)}>
            <div className="modal-header justify-content-center">
                <button className="close" onClick={() => setDemoModal(false)}>
                    <i className="tim-icons icon-simple-remove text-white" />
                </button>
                <h4 className="title title-up">{activeAbfallsorte.name}</h4>
            </div>
            <div className="modal-body">
                <Row>
                    <Col sm="6" className="blockquoteAbfaelle blockquoteAbfaelle-success">
                            <div>
                            <blockquote>
                                    <small>- Do's &nbsp;<i className="tim-icons icon-check-2"/></small><br/><br/>
                                    <ul>
                                    {activeAbfallsorte.dos.map((e)=>{return(<li>{e}</li>)})}
                                    </ul>
                            </blockquote>
                            </div>
                    </Col>
                    <Col sm="6" className="blockquoteAbfaelle blockquoteAbfaelle-danger">
                            <div>
                            <blockquote>        
                                    <small>- Dont's &nbsp;<i className="tim-icons icon-simple-remove"/></small><br/><br/>
                                    <ul>
                                    {activeAbfallsorte.donts.map((e)=>{return(<li>{e}</li>)})}    
                                    </ul>       
                            </blockquote>
                            </div>
                    </Col>
                </Row>

            </div>
            <div className="modal-footer">
                <Button color="info" type="button" onClick={() => {
                    setActiveAbfallsorteKey(activeAbfallsorte.id);
                    props.changeActiveAbfallsorte(activeAbfallsorte.id);
                    setDemoModal(false)
                }}>
                    auswählen
                </Button>
                <Button
                    color="danger"
                    type="button"
                    onClick={() => setDemoModal(false)}>
                    schließen
                </Button>
            </div>
        </Modal>
    );



    return (
        <Row>
            {abfallsorten.map((abfallsorte) => {
                let key = abfallsorte.id;
                return (
                    <Col sm="3">
                        <Card 
                            onClick={() => { setActiveAbfallsorteKey(key); props.changeActiveAbfallsorte(key) }}
                            className={
                            `btn card-abfallsorte-btnstyle abfall card-abfallsorte ${activeAbfallsorteKey === key ? 'card-abfallsorte-active' : ''}`}>
                            <CardImg variant="top" src={abfallsorte.src} />
                            <CardBody>
                                <CardTitle><strong>{abfallsorte.name}</strong></CardTitle>
                                <CardText>
                                    {abfallsorte.beschreibung}
                                </CardText>
                                <Button className="btn-link abfall card-abfallsorte-auswahl" color="info"
                                    onClick={() => { setActiveAbfallsorteKey(key); props.changeActiveAbfallsorte(key) }}>
                                    auswählen
                                </Button>
                                <Button className="btn-link" color="success"
                                    onClick={() => { setActiveAbfallsorte(abfallsorte); setDemoModal(true); }}>
                                    was darf rein  &nbsp;
                                    <i aria-hidden={true} className="tim-icons icon-minimal-right" />
                                </Button>
                            </CardBody>
                        </Card>
                    </Col>
                );
            })}
            {modal()}
        </Row>
    );
}