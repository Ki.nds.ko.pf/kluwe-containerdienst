import React, { useEffect, state } from "react";

import ReactDatetime from "react-datetime";
import 'moment/locale/de';
import DatePicker from "react-datepicker";
import { DayPicker } from 'react-day-picker';
// import 'react-datepicker/dist/react-datepicker.css'
import "assets/css/react-day-picker.css";
import moment from 'moment';
import Calendly from "./Calendly.js"

import {
    Modal,
    Row,
    Col,
    Card,
    CardBody,
    CardImg,
    CardTitle,
    CardText,
    FormGroup,
    Input,
    Label,
    Button,
    UncontrolledAlert,
} from "reactstrap";


export default function Anfrage(props) {

    if (props.abfallsorte.id && props.container.id) {
        return (
            <div>
                <p>Wichtig! Abfallsorte und Container nur in den Schritten vorher ändern sonst können wir nicht gewährleisten das die abfallsorte zu containerauwahl passt.</p>
                <p>Bitte maße lkw</p>
                <Calendly abfallsorte={props.abfallsorte} container={props.container}></Calendly>
            </div>
        );
    } else if (!props.abfallsorte.id) {
        return (
            <UncontrolledAlert className="alert-with-icon" color="default">
                <span data-notify="icon" className="tim-icons icon-support-17" />
                <span>
                    <b>Bitte wähle einen Abfallsorte auß -</b>
                    <Button className="btn-link" color="default"
                        onClick={(e) => { props.changeActiveTab(1) }}>
                        zurück zu Abfallsorte
                    </Button>
                </span>
            </UncontrolledAlert>
        );
    } else if (!props.container.id) {
        return (
            <UncontrolledAlert className="alert-with-icon" color="default">
                <span data-notify="icon" className="tim-icons icon-support-17" />
                <span>
                    <b>Bitte wähle einen Container auß -</b>
                    <Button className="btn-link" color="default"
                        onClick={(e) => { props.changeActiveTab(2) }}>
                        zurück zu Container
                    </Button>
                </span>
            </UncontrolledAlert>
        );
    }
}